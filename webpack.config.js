const path = require('path');
const webpack = require('webpack');

module.exports = {
	entry: "./app",
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "bundle.js",
	},

	module: {
		rules: [
			{
				test: /\.jsx?$/,
				include: [
					path.resolve(__dirname, "app")
				],
				loader: "babel-loader",
				options: {
					presets: ["es2015"]
				},
			},
			{
				test: /\.html$/,
				use: [
					"htmllint-loader",
					{
						loader: "html-loader",
					}
				]
			},
			{
				test: /\.scss$/,
				use: [{
						loader: "style-loader"
				}, {
						loader: "css-loader"
				}, {
						loader: "sass-loader"
				}]
			}
		]
	},

	resolve: {

		modules: [
			"node_modules",
			path.resolve(__dirname, "app")
		],
		extensions: [".js", ".json", ".jsx", ".css"],
		alias: {
			"module": "new-module",
			"only-module$": "new-module",
			"module": path.resolve(__dirname, "app/third/module.js"),
		},
	},
	context: __dirname,
	target: "web",
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		port: 1111
	},
	plugins: [
		// new webpack.DefinePlugin({
		//   'process.env': {
		//     NODE_ENV: JSON.stringify('production')
		//   }
		// }),
		// new webpack.optimize.UglifyJsPlugin(),
			new webpack.ProvidePlugin({
					"React": "react",
			}),
		],
}